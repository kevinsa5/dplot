from design import Ui_MainWindow
import pandas as pd
from PyQt4 import QtGui, QtCore
import sys

class StreamRedirect(object):
    def __init__(self, textedit):
        self.textedit = textedit

    def write(self, text):
        self.textedit.moveCursor(QtGui.QTextCursor.End)
        self.textedit.insertPlainText(text)


class MainWindow(QtGui.QMainWindow, Ui_MainWindow):
    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)
        self.setupUi(self)
        self.setWindowTitle('DATAPLOT')
        
        self.setupMenu()
        self.setupControls()

    def setupMenu(self):
        self.actionLoad_data_file.triggered.connect(self._browseInputFile)

    def setupControls(self):

        def update_data():
            xvar = str(self.xVariableComboBox.currentText())
            yvar = str(self.yVariableComboBox.currentText())
            cvar = str(self.cVariableComboBox.currentText())
            cmap = str(self.colorMapComboBox.currentText())
            self.graphicsView.set_data(xvar, yvar, cvar, cmap)

        self.xVariableComboBox.activated.connect(update_data)
        self.yVariableComboBox.activated.connect(update_data)
        self.cVariableComboBox.activated.connect(update_data)
        self.colorMapComboBox.activated.connect(update_data)
        
        self.colorMapComboBox.addItems(['viridis', 'magma', 'plasma', 'inferno'])

    def _browseInputFile(self):
        filename = QtGui.QFileDialog.getOpenFileName(self, "Select a data file")
        filename = str(filename)
        if filename:
            print 'Loading data file:', filename 
            df = pd.read_csv(filename, index_col = 0, parse_dates = True)
            self.graphicsView.df = df
            self.xVariableComboBox.clear()
            self.yVariableComboBox.clear()
            self.cVariableComboBox.clear()
            print 'Found columns:', df.columns.tolist()
            for box in [self.xVariableComboBox, self.yVariableComboBox, self.cVariableComboBox]:
                box.clear()
                for col in df.columns:
                    box.addItem(col)


def main():
    app = QtGui.QApplication([])
    ui = MainWindow()
    redirect = StreamRedirect(ui.stdoutTextEdit)
    sys.stdout = redirect
    sys.stderr = redirect
    print "UI Initialized; executing QT Application"
    ui.show()
    app.exec_()

if __name__ == '__main__':
    main()
