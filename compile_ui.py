import os
from PyQt4 import uic

SRC = './ui'
DEST = '.'

if __name__ == '__main__':
    uic.compileUiDir(SRC, map=lambda src, name: (DEST, name))
