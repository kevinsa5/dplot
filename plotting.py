import pyqtgraph as pg
from pyqtgraph.Qt import QtGui
import numpy as np
import matplotlib.cm as mplcm

class PlotWidget(pg.GraphicsWindow):
    pg.setConfigOption('background', 'w')
    pg.setConfigOption('foreground', 'k')
    
    def __init__(self, parent=None, **kargs):
        pg.GraphicsWindow.__init__(self, **kargs)
        self.setParent(parent)
        self.xdata = np.array([]) 
        self.ydata = np.array([])
        self.cdata = np.array([])
        self.plot = self.addPlot(labels =  {'left':'-', 'bottom':'-'})
        self.scatter = self.plot.plot(x=self.xdata, y=self.ydata)
        self.tooltip = pg.TextItem(text="", color=(0,0,0), anchor=(1,1))
        self.tooltip.hide()
        self.plot.addItem(self.tooltip)
        self.plot.scene().sigMouseMoved.connect(self.onMove)

    def onMove(self, pos):
        print pos
        action_pos = self.plot.vb.mapSceneToView(pos)
        print action_pos
        hovered_points = self.scatter.scatter.pointsAt(action_pos)
        if len(hovered_points) == 0:
            self.tooltip.hide()
        else:
            # only display one point for now
            point = hovered_points[0]
            pos = point.pos()
            x,y = pos.x(), pos.y()
            self.tooltip.setText('x=%f\ny=%f' % (x,y))
            self.tooltip.setPos(x,y)
            self.tooltip.show()

    def cmap(self, cmap_name, cdata):
        cmap = mplcm.get_cmap(cmap_name)
        colordata = getattr(cmap, 'colors')
        indices = np.linspace(0., 1., len(colordata))
        cm = pg.ColorMap(pos = indices, color = colordata)
        # normalize to [0,1]
        cdata = (cdata - cdata.min()) * 1.0 / (cdata.max() - cdata.min())
        return cm.mapToQColor(cdata)

    def set_data(self, x, y, c, cmap_name='viridis'): 
        log_vars = ['x','y','c','cmap_name']
        print "plot data updating:", " ".join(["{}:{}".format(key, locals()[key]) for key in log_vars])
        self.xdata = np.array(self.df[x])
        self.ydata = np.array(self.df[y])
        self.cdata = np.array(self.df[c])

        brushes = self.cmap(cmap_name, self.cdata)

        self.scatter.setData(x=self.xdata, y=self.ydata, symbol='o', pen=None, symbolBrush=brushes)
        self.plot.setLabels(bottom=x, left=y)
        

if __name__ == '__main__':
    w = PlotWidget()
    w.show()
    QtGui.QApplication.instance().exec_()
